SimpleAFIS by Gede Indrawan was an online fingerprint recognition demo using SourceAFIS as its backend.
It was developed in ASP.NET and data is stored in SQL Server database that can be found in App_Data folder.
It compiled and ran, but it's somewhat buggy. I was able to enroll fingerprints, but I couldn't match them.
You might need to do some fixing to get it working. It could use upgrade to latest SourceAFIS.

Setup steps:

1. Create directories deps and Images if they don't exist already.
2. Copy SourceAFIS.dll to deps folder.
3. Build using VS 2010 or higher. Express edition is sufficient.
4. Run SQL Server Express 2008 or higher. No need to setup database nor admin access. Just have SQL Server running.
5. Run the app and log in using credentials that can be found in adminpass.txt.

Questions should be directed to [Gede Indrawan](https://bitbucket.org/gindrawan).
