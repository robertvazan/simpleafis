﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SAFIS.BLL;
using SourceAFIS.Simple;
using System.Windows.Media.Imaging;

namespace SAFIS
{
  public partial class _Default : System.Web.UI.Page
  {
		// A page variable to "remember" the deleted fingerprint file
		string deletedFingerPrintPath;

		// A page variable for the result of matched fingerprint
		int fingerPrintID;

		// Inherit from Fingerprint in order to add Filename field
		[Serializable]
		class MyFingerprint : Fingerprint
		{
			public string Filename;
		}

		// Inherit from Person in order to add Name field
		[Serializable]
		class MyPerson : Person
		{
			public string Name;
		}

		// Shared AfisEngine instance (cannot be shared between different threads though)
		static AfisEngine Afis;

		protected void Page_Load(object sender, EventArgs e)
		{
			ExceptionDetails.Visible = false;
		}

    protected void NewFingerPrint_ItemInserting(object sender, DetailsViewInsertEventArgs e)
    {
      // Reference the FileUpload controls
      FileUpload insertFingerPrintImageFileUpload = (FileUpload)NewFingerPrint.FindControl("InsertFingerPrintImageFileUpload");

			//// Reference the PersonName controls
			//System.Web.UI.WebControls.BoundField personNameBoundField = (System.Web.UI.WebControls.BoundField)NewFingerPrint.FindControl("PersonName");

			string personName = "Mr. X";
			//if (personNameBoundField.DataField != null)
			//  personName = Convert.ToString(e.Values["personName"]);
			if (e.Values["PersonName"] != null)
				personName = Convert.ToString(e.Values["PersonName"]);


			if (insertFingerPrintImageFileUpload.HasFile)
      {
				// Make sure the picture upload is valid
				if (ValidFingerPrintUpload(insertFingerPrintImageFileUpload))
				{
					const string FingerPrintDirectory = "~/Images/";
					//string fingerPrintPath = FingerPrintDirectory + insertFingerPrintImageFileUpload.FileName;
					deletedFingerPrintPath = FingerPrintDirectory + insertFingerPrintImageFileUpload.FileName;
					string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(insertFingerPrintImageFileUpload.FileName);
					string fileNameExtension = Path.GetExtension(insertFingerPrintImageFileUpload.FileName);

					int iteration = 1;
					while (File.Exists(Server.MapPath(deletedFingerPrintPath)))
					{
						deletedFingerPrintPath = string.Concat(FingerPrintDirectory, fileNameWithoutExtension, "-", iteration, fileNameExtension);
							iteration++;
					}

					// Save the file to disk
					insertFingerPrintImageFileUpload.SaveAs(Server.MapPath(deletedFingerPrintPath));

					// Set the value of FingerPrint parameters
					//e.Values["fingerPrintPath"] = deletedFingerPrintPath;
					//e.Values["fingerPrintImage"] = insertFingerPrintImageFileUpload.FileBytes;

					ProbeFingerPrintName.Visible = true;
					ProbeFingerPrintName.Text = "Fingerprint of " + personName;
					ProbeFingerPrintImage.Visible = true;
					//ProbeFingerPrintImage.ImageUrl = "~/Thumbnailer.ashx?file=" + Server.UrlEncode(insertFingerPrintImageFileUpload.FileName);
					//ProbeFingerPrintImage.ImageUrl = "~/Thumbnailer.ashx?file=" + deletedFingerPrintPath;
					ProbeFingerPrintImage.ImageUrl = "~/DisplayFingerPrintImage.aspx?ProbeFingerPrint=" + deletedFingerPrintPath;
				}
        else
        {
          // Invalid file upload, cancel insert and exit event handler
          e.Cancel = true;
          return;
        }
      }
      else
      {
        // No picture uploaded!
        ExceptionDetails.Visible = true;
        ExceptionDetails.Text = "You must provide an image for identification.";
        e.Cancel = true;
        return;
      }

			// Display a user-friendly message
			ExceptionDetails.Visible = true;
			ExceptionDetails.Text = "Identifying result: ";

			// Initialize SourceAFIS
      Afis = new AfisEngine();

      // Enroll some people
      FingerPrintsBLL fingerPrintBLL = new FingerPrintsBLL();
      SAFIS.DAL.SAFIS.FingerPrintsDataTable fingerPrintsDataTable = fingerPrintBLL.GetFingerPrints();

			PersonsBLL personsBLL = new PersonsBLL();
      List<MyPerson> database = new List<MyPerson>();
             
      for (int i = 0; i <= fingerPrintsDataTable.Count-1; i++)
      {
          SAFIS.DAL.SAFIS.FingerPrintsRow fingerPrintsRow = fingerPrintsDataTable[i];
          SAFIS.DAL.SAFIS.PersonsDataTable personsDataTable = personsBLL.GetPersonsByPersonID(fingerPrintsRow.PersonID);
          database.Add(Enroll(personsDataTable[0].PersonID, Server.MapPath(fingerPrintsRow.FingerPrintPath), personsDataTable[0].PersonName));
      }

			//string personName = "Mr. X";
			//if (e.Values["personName"] != null)
			//  personName = Convert.ToString(e.Values["personName"]);
			//ProbeFingerPrintName.Visible = true;
			//ProbeFingerPrintName.Text = "Fingerprint of " + personName;

      // Enroll visitor with unknown identity
			string probeFingerPrintPath = Server.MapPath(deletedFingerPrintPath);
      MyPerson probe = Enroll(0, probeFingerPrintPath, personName);

      // Look up the probe using Threshold = 10
      Afis.Threshold = 10;

      //Console.WriteLine("Identifying {0} in database of {1} persons...", probe.Name, database.Count);
			//MyPerson match = Afis.Identify(probe, database);
			MyPerson match = Afis.Identify(probe, database) as MyPerson;

      // Null result means that there is no candidate with similarity score above threshold
      if (match == null)
      {
        //Console.WriteLine("No matching person found.");
        //throw new ApplicationException("No matching fingerprint found.");
				ExceptionDetails.Visible = true;
				ExceptionDetails.Text += " No matching fingerprint found.";
				e.Cancel = true;
				return;
			}

      // Print out any non-null result
      //Console.WriteLine("Probe {0} matches registered person {1}", probe.Name, match.Name);
			// Still no accomodate if more than one fingerprints of same person enroll in database
			fingerPrintsDataTable = fingerPrintBLL.GetFingerPrintsByPersonID(match.Id);
      fingerPrintID = fingerPrintsDataTable[0].FingerPrintID;

			//for (int i = 0; i <= fingerPrintsDataTable.Count-1; i++)
      //{
      //    fingerPrintID = fingerPrintsDataTable[i].FingerPrintID;
      //}

      // Compute similarity score
      float score = Afis.Verify(probe, match);
      //Console.WriteLine("Similarity score between {0} and {1} = {2:F3}", probe.Name, match.Name, score);
			//fingerPrintsDataTable[0].MatchingScore = score.ToString();
			ExceptionDetails.Visible = true;
			ExceptionDetails.Text += " Matching Score = " + score.ToString();
		}

    protected void NewFingerPrint_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
    {
			//DeleteRememberedFingerPrintPath();

			if (e.Exception != null)
      {
        if (e.Exception.InnerException != null)
        {
          Exception inner = e.Exception.InnerException;

          if (inner is System.Data.Common.DbException)
            ExceptionDetails.Text += "Our database is currently experiencing problems. Please try again later.";
          else if (inner is NoNullAllowedException)
            ExceptionDetails.Text += "There are one or more required fields that are missing.";
          else if (inner is ArgumentException)
          {
            string paramName = ((ArgumentException)inner).ParamName;
            ExceptionDetails.Text += string.Concat("The ", paramName, " value is illegal.");
          }
          else if (inner is ApplicationException)
            ExceptionDetails.Text += inner.Message;
        }

        // Indicate that the exception has been handled
        e.ExceptionHandled = true;

        // Keep the row in edit mode
        e.KeepInInsertMode = true;
      }
		}

    protected void FingerPrintsDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        e.InputParameters["fingerPrintID"] = fingerPrintID;
    }

    protected void FingerPrintsDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
    {
			//DeleteRememberedFingerPrintPath();
		}

		protected string GenerateFingerPrintLink(object fingerPrintPath)
		{
			//if (Convert.IsDBNull(fingerPrintPath))
			//  return "No FingerPrint Available";
			//else
			//  //return string.Format(@"<a href=""{0}"">View FingerPrint</a>", ResolveUrl(FingerPrintPath.ToString()));
			return string.Format(@"{0}", ResolveUrl(fingerPrintPath.ToString()));
		}

		//protected string GenerateProbeFingerPrintLink()
		//{
		//  //if (Convert.IsDBNull(fingerPrintPath))
		//  //  return "No FingerPrint Available";
		//  //else
		//  //  //return string.Format(@"<a href=""{0}"">View FingerPrint</a>", ResolveUrl(FingerPrintPath.ToString()));
		//  return string.Format(@"{0}", ResolveUrl(deletedFingerPrintPath.ToString()));
		//}

		private bool ValidFingerPrintUpload(FileUpload fileUpload)
		{
			// Make sure that a JPG, TIF has been uploaded
			if (string.Compare(Path.GetExtension(fileUpload.FileName), ".jpg", true) != 0 &&
					string.Compare(Path.GetExtension(fileUpload.FileName), ".jpeg", true) != 0 &&
					string.Compare(Path.GetExtension(fileUpload.FileName), ".tif", true) != 0 &&
					string.Compare(Path.GetExtension(fileUpload.FileName), ".tiff", true) != 0)
			{
				ExceptionDetails.Text = "Only JPG, TIF formats may be used.";
				ExceptionDetails.Visible = true;
				return false;
			}
			else
			{
				return true;
			}
		}

		private void DeleteRememberedFingerPrintPath()
		{
			// Is there a file to delete?
			if (deletedFingerPrintPath != null)
			{
				File.Delete(Server.MapPath(deletedFingerPrintPath));
			}
		}

		// Take fingerprint image file and create Person object from the image
		static MyPerson Enroll(int personID, string filename, string name)
		{
			//Console.WriteLine("Enrolling {0}...", name);

			// Initialize empty fingerprint object and set properties
			MyFingerprint fp = new MyFingerprint();
			fp.Filename = filename;

			// Load image from the file, fp.BitmapImage makes a copy of the image, so that we can dispose it afterwards
			//Console.WriteLine(" Loading image from {0}...", filename);
			//using (System.Drawing.Image fromFile = System.Drawing.Bitmap.FromFile(filename))
			//using (System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(fromFile))
			//fp.BitmapImage = bitmap;
			BitmapImage image = new BitmapImage(new Uri(filename, UriKind.RelativeOrAbsolute));
			fp.AsBitmapSource = image;

			// Above update of fp.BitmapImage initialized also raw image in fp.Image
			// Check raw image dimensions, Y axis is first, X axis is second
			//Console.WriteLine(" Image size = {0} x {1} (width x height)", fp.Image.GetLength(1), fp.Image.GetLength(0));

			// Execute extraction in order to initialize fp.Template
			//Console.WriteLine(" Extracting template...");
			//Afis.Extract(fp);

			// Check template size
			//Console.WriteLine(" Template size = {0} bytes", fp.Template.Length);

			// Initialize empty person object and set its properties
			MyPerson person = new MyPerson();
			person.Name = name;
			person.Id = personID;

			// Add fingerprint to the person
			//person.Add(fp);
			person.Fingerprints.Add(fp);

			Afis.Extract(person);
			return person;
		}

  }
}