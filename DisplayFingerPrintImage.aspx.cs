﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using SAFIS.BLL;

namespace SAFIS
{
  public partial class DisplayFingerPrintImage : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
			string fingerPrintPath = null;

			if (Request.QueryString["FingerPrintID"] != null)
			{
				int fingerPrintID = Convert.ToInt32(Request.QueryString["FingerPrintID"]);
				// Get information about the specified fingerprint
				FingerPrintsBLL fingerPrintBLL = new FingerPrintsBLL();
				SAFIS.DAL.SAFIS.FingerPrintsDataTable fingerPrintsDataTable = fingerPrintBLL.GetFingerPrintsByFingerPrintID(fingerPrintID);
				SAFIS.DAL.SAFIS.FingerPrintsRow fingerPrintsRow = fingerPrintsDataTable[0];
				fingerPrintPath = fingerPrintsRow.FingerPrintPath;
			}
			else if (Request.QueryString["ProbeFingerPrint"] != null)
			{
				fingerPrintPath = Convert.ToString(Request.QueryString["ProbeFingerPrint"]);
			}

			// Work for any kind of image but TIF
			//Response.BinaryWrite(fingerPrint.FingerPrintImage);

			try
			{
				// Without thumbnailing
				//Stream stream = new FileStream(Server.MapPath(fingerPrintPath), FileMode.Open);
				//Bitmap bitmap = new Bitmap(stream);

				// With thumbnailing
				Image.GetThumbnailImageAbort callback = null;
				IntPtr callbackData = IntPtr.Zero;
				Image image = Image.FromFile(Server.MapPath(fingerPrintPath));
				Image thumbnail = image.GetThumbnailImage(100, 100, callback, callbackData);

				// Output HTTP headers providing information about the binary data
				Response.ContentType = "image/jpg";

				// Without thumbnailing
				//bitmap.Save(Response.OutputStream, ImageFormat.Jpeg);
				//bitmap.Dispose();
				//stream.Close();

				// With thumbnailing
				thumbnail.Save(Response.OutputStream, ImageFormat.Jpeg);
			}
			catch
			{
			}

			//int fingerPrintID = Convert.ToInt32(Request.QueryString["FingerPrintID"]);

			//// Get information about the specified fingerprint
			//FingerPrintsBLL fingerPrintBLL = new FingerPrintsBLL();
			//SAFIS.DAL.SAFIS.FingerPrintsDataTable fingerPrintsDataTable = fingerPrintBLL.GetFingerPrintsByFingerPrintID(fingerPrintID);
			//SAFIS.DAL.SAFIS.FingerPrintsRow fingerPrintsRow = fingerPrintsDataTable[0];

			//// Work for any kind of image but TIF
			////Response.BinaryWrite(fingerPrint.FingerPrintImage);

			//try
			//{
			//  Stream stream = new FileStream(Server.MapPath(fingerPrintsRow.FingerPrintPath), FileMode.Open);
			//  Bitmap bitmap = new Bitmap(stream);

			//  // Output HTTP headers providing information about the binary data
			//  Response.ContentType = "image/jpg";

			//  bitmap.Save(Response.OutputStream, ImageFormat.Jpeg);
			//  bitmap.Dispose();
			//  stream.Close();
			//}
			//catch
			//{
			//}
    }

	}
}