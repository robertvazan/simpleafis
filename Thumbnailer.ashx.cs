﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;

using System.IO;
//using System.Web.UI;

namespace SAFIS
{
	/// <summary>
	/// Summary description for Thumbnailer
	/// </summary>
	public class Thumbnailer : IHttpHandler
	{

		public void ProcessRequest(HttpContext context)
		{
			//string imgpath = "";
			//Image.GetThumbnailImageAbort callback = null;
			//IntPtr callbackData = IntPtr.Zero;
			//Image thumbnailimg;
			//Image largeimg = null;
			//if (context.Request["file"] != null)
			//{
			//  imgpath = context.Server.UrlDecode(context.Request["file"]);
			//}
			//try
			//{
			//  largeimg = Image.FromFile(context.Server.MapPath("~/Images/" + imgpath));
			//}
			//catch (Exception exc)
			//{
			//  largeimg = Image.FromFile(context.Server.MapPath("~/Images/notfound.jpg"));
			//}
			//thumbnailimg = largeimg.GetThumbnailImage(100, 100, callback, callbackData);
			//context.Response.ContentType = "image/Jpeg";
			//thumbnailimg.Save(context.Response.OutputStream, ImageFormat.Jpeg);

			try
			{
				//Stream stream = new FileStream(Server.MapPath(fingerPrintsRow.FingerPrintPath), FileMode.Open);
				Stream stream = new FileStream(context.Server.MapPath(context.Request["file"]), FileMode.Open);
				Bitmap bitmap = new Bitmap(stream);

				// Output HTTP headers providing information about the binary data
				//Response.ContentType = "image/jpg";
				context.Response.ContentType = "image/Jpeg";

				//bitmap.Save(Response.OutputStream, ImageFormat.Jpeg);
				bitmap.Save(context.Response.OutputStream, ImageFormat.Jpeg);
				bitmap.Dispose();
				stream.Close();
			}
			catch
			{
			}
		}

		public bool IsReusable
		{
			get
			{
				return false;
			}
		}
	}
}