﻿<%@ Page Title="Identify" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
	CodeBehind="Default.aspx.cs" Inherits="SAFIS._Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
  <%--<p>
    Insert a Fingerprint Image File for Identification.</p>--%>

  <p>
    <asp:Label ID="IdentifyLabel" runat="server" CssClass="Warning">
		Insert a Fingerprint Image for Identification</asp:Label></p>

  <p>
    <asp:DetailsView ID="NewFingerPrint" runat="server" AutoGenerateRows="False" 
      DataKeyNames="FingerPrintID" DataSourceID="FingerPrintsDataSource" 
      DefaultMode="Insert" OnItemInserting="NewFingerPrint_ItemInserting" OnItemInserted="NewFingerPrint_ItemInserted">
      <Fields>
        <asp:BoundField DataField="FingerPrintID" HeaderText="FingerPrint ID" 
          InsertVisible="False" ReadOnly="True" SortExpression="FingerPrintID" />
        <asp:BoundField DataField="PersonID" HeaderText="Person ID" 
          InsertVisible="False" SortExpression="PersonID" />
        <asp:BoundField DataField="PersonName" HeaderText="Person Name" 
          SortExpression="PersonName" />
        <asp:BoundField DataField="FingerID" HeaderText="Finger ID" 
          InsertVisible="False" SortExpression="FingerID" />
        <asp:BoundField DataField="FingerName" HeaderText="Finger Name" 
          InsertVisible="False" SortExpression="FingerName" />
        <asp:TemplateField HeaderText="Finger Type">
          <InsertItemTemplate>
            <asp:DropDownList ID="InsertFingersDropDownList" runat="server" AutoPostBack="True" 
              DataSourceID="FingersDataSource" DataTextField="FingerName" 
              DataValueField="FingerID">
            </asp:DropDownList>
            <asp:ObjectDataSource ID="FingersDataSource" runat="server" 
              SelectMethod="GetFingers" TypeName="SAFIS.BLL.FingersBLL">
            </asp:ObjectDataSource>
          </InsertItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="NumberOfScan" HeaderText="Number Of Scan" 
          InsertVisible="False" SortExpression="NumberOfScan" />
        <asp:BoundField DataField="FingerPrintPath" HeaderText="FingerPrint Path" 
          InsertVisible="False" SortExpression="FingerPrintPath" />
        <asp:TemplateField HeaderText="FingerPrint Image">
          <InsertItemTemplate>
            <asp:FileUpload ID="InsertFingerPrintImageFileUpload" runat="server" />
          </InsertItemTemplate>
        </asp:TemplateField>
        <asp:CommandField ShowInsertButton="True" />
      </Fields>
    </asp:DetailsView>
  </p>

  <%-- No need to write code at Page_Load event --%>
  <%--<p>
    <asp:Label ID="ExceptionDetails" runat="server" CssClass="Warning" 
			EnableViewState="False" Visible="False"></asp:Label></p>--%>

  <p>
    <asp:Label ID="ProbeFingerPrintName" runat="server" CssClass="Warning"
			EnableViewState="False" Visible="False"></asp:Label></p>

  <p>
		<asp:Image ID="ProbeFingerPrintImage" runat="server" EnableViewState="False" Visible="False" /></p>

  <%-- Alternatif to code above --%>
  <%--<p>
		<asp:ImageButton ID="ProbeFingerPrintImage" runat="server" Visible="False" /></p>--%>

  <%-- Need to write code at Page_Load event --%>
  <p>
    <asp:Label ID="ExceptionDetails" runat="server" CssClass="Warning"></asp:Label></p>

  <p>
    <asp:GridView ID="FingerPrints" runat="server" 
      DataSourceID="FingerPrintsDataSource" AutoGenerateColumns="False" 
      DataKeyNames="FingerPrintID" EnableViewState="False" AllowPaging="True">
      <Columns>
        <asp:BoundField DataField="FingerPrintID" HeaderText="FingerPrint ID" 
          InsertVisible="False" ReadOnly="True" SortExpression="FingerPrintID" >
        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
        </asp:BoundField>
        <asp:BoundField DataField="PersonID" HeaderText="Person ID" 
          Visible="False" SortExpression="PersonID" >
        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
        </asp:BoundField>
        <%--<asp:BoundField DataField="PersonName" HeaderText="Person Name" 
          SortExpression="PersonName" />--%>
        <asp:TemplateField HeaderText="Person Name" SortExpression="PersonName">
          <EditItemTemplate>
            <asp:TextBox ID="PersonNameTextBox" runat="server" Text='<%# Bind("PersonName") %>'></asp:TextBox>
          </EditItemTemplate>
          <ItemTemplate>
            <asp:Label ID="PersonNameLabel" runat="server" Text='<%# Bind("PersonName") %>'></asp:Label>
          </ItemTemplate>
          <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
        </asp:TemplateField>
        <asp:BoundField DataField="FingerID" HeaderText="Finger ID" 
          InsertVisible="False" Visible="False" SortExpression="FingerID" />
        <asp:BoundField DataField="FingerName" HeaderText="Finger Name" 
          InsertVisible="False" Visible="False" SortExpression="FingerName" />
        <asp:TemplateField HeaderText="Finger Type" SortExpression="FingerID">
          <EditItemTemplate>
            <asp:DropDownList ID="EditFingersDropDownList" runat="server" AutoPostBack="True" 
              DataSourceID="FingersDataSource" DataTextField="FingerName" 
              DataValueField="FingerID">
            </asp:DropDownList>
            <asp:ObjectDataSource ID="FingersDataSource" runat="server" 
              SelectMethod="GetFingers" TypeName="SAFIS.BLL.FingersBLL">
            </asp:ObjectDataSource>
          </EditItemTemplate>
          <ItemTemplate>
            <asp:Label ID="FingerNameLabel" runat="server" Text='<%# Bind("FingerName") %>'></asp:Label>
          </ItemTemplate>
          <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
        </asp:TemplateField>
        <%--<asp:BoundField DataField="NumberOfScan" HeaderText="Number Of Scan" 
          InsertVisible="False" SortExpression="NumberOfScan" >
        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
        </asp:BoundField>--%>
        <asp:TemplateField HeaderText="Number Of Scan" InsertVisible="False" 
          SortExpression="NumberOfScan">
          <ItemTemplate>
            <asp:Label ID="NumberOfScanLabel" runat="server" Text='<%# Bind("NumberOfScan") %>'></asp:Label>
          </ItemTemplate>
          <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
        </asp:TemplateField>
        <%--<asp:BoundField DataField="FingerPrintPath" HeaderText="FingerPrint Path" 
          InsertVisible="False" SortExpression="FingerPrintPath" >
        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
        </asp:BoundField>--%>
        <%--<asp:HyperLinkField DataNavigateUrlFields="FingerPrintPath" 
          HeaderText="FingerPrint Path" Text="View FingerPrint" />--%>
        <asp:TemplateField HeaderText="FingerPrint Path" InsertVisible="False" 
          SortExpression="FingerPrintPath">
          <ItemTemplate>
            <%--<asp:Label ID="FingerPrintPathLabel" runat="server" Text='<%# Bind("FingerPrintPath") %>'></asp:Label>--%>
            <%# GenerateFingerPrintLink(Eval("FingerPrintPath")) %>
          </ItemTemplate>
          <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
        </asp:TemplateField>
        <%--<asp:ImageField DataImageUrlField="FingerPrintID" 
          DataImageUrlFormatString="~/DisplayFingerPrintImage.aspx?FingerPrintID={0}" 
          HeaderText="FingerPrint Image">
        </asp:ImageField>--%>
        <asp:TemplateField HeaderText="FingerPrint Image">
          <EditItemTemplate>
            <asp:Image ID="FingerPrintImageImage" runat="server" 
              ImageUrl='<%# Eval("FingerPrintID", "~/DisplayFingerPrintImage.aspx?FingerPrintID={0}") %>' /><br />
							To change the fingerprint's image, specify a new image. To keep the fingerprint's 
							image the same, leave the field empty.<br />
            <asp:FileUpload ID="EditFingerPrintImageFileUpload" runat="server" />
          </EditItemTemplate>
          <ItemTemplate>
            <asp:Image ID="FingerPrintImageImage" runat="server" 
              ImageUrl='<%# Eval("FingerPrintID", "~/DisplayFingerPrintImage.aspx?FingerPrintID={0}") %>' />
          </ItemTemplate>
          <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
        </asp:TemplateField>
        <%--<asp:BoundField DataField="MatchingScore" HeaderText="Matching Score" 
          SortExpression="MatchingScore" >
        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
        </asp:BoundField>--%>
      </Columns>
    </asp:GridView>

    <asp:ObjectDataSource ID="FingerPrintsDataSource" runat="server" 
      OldValuesParameterFormatString="{0}" 
      SelectMethod="GetFingerPrintsByFingerPrintID" TypeName="SAFIS.BLL.FingerPrintsBLL" 
      onselecting="FingerPrintsDataSource_Selecting" 
      onselected="FingerPrintsDataSource_Selected" 
      InsertMethod="IdentifyFingerPrints">
      <InsertParameters>
        <asp:Parameter Name="personName" Type="String" />
        <asp:Parameter Name="fingerID" Type="Int32" />
        <asp:Parameter Name="fingerPrintPath" Type="String" />
        <asp:Parameter Name="fingerPrintImage" Type="Object" />
      </InsertParameters>
      <SelectParameters>
        <asp:Parameter Name="fingerPrintID" Type="Int32"/>
      </SelectParameters>
    </asp:ObjectDataSource>
  </p>

  <%--<p>
		<asp:FileUpload ID="IdentifyFingerprintImageFileUpload" runat="server" /></p>
  <p>
		<asp:Button ID="IdentifyFingerprintImageButton" runat="server" OnClick="IdentifyFingerprintImageButton_Click" Text="Identify Selected File" />&nbsp;</p>
  <p>
		<asp:Label ID="IdentifyFingerprintImageLabel" runat="server"></asp:Label>&nbsp;</p>--%>

  <%--<p>
		<asp:Label ID="UploadWarning" runat="server" CssClass="Warning" 
			EnableViewState="False" Visible="False"></asp:Label></p>--%>

</asp:Content>