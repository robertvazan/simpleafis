﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SAFIS.DAL.SAFISTableAdapters;
using System.ComponentModel;

namespace SAFIS.BLL
{
	[System.ComponentModel.DataObject]
	public class PersonsBLL
	{
		private PersonsTableAdapter _personsAdapter = null;

		protected PersonsTableAdapter Adapter
		{
			get
			{
				if (_personsAdapter == null)
					_personsAdapter = new PersonsTableAdapter();

				return _personsAdapter;
			}
		}

		[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
		public SAFIS.DAL.SAFIS.PersonsDataTable GetPersonsByPersonID(int personID)
		{
			return Adapter.GetPersonsByPersonID(personID);
		}

		[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
		public SAFIS.DAL.SAFIS.PersonsDataTable GetMaxPersonID()
		{
			return Adapter.GetMaxPersonID();
		}

		//[DataObjectMethodAttribute(DataObjectMethodType.Insert, false)]
		//public int InsertPersonsByPersonName(string personName)
		//{
		//    return Adapter.InsertPersonsByPersonName(personName);
		//}

		[DataObjectMethodAttribute(DataObjectMethodType.Insert, false)]
		public void InsertPersons(string personName)
		{
			Adapter.Insert(personName);
		}

		[DataObjectMethodAttribute(DataObjectMethodType.Update, false)]
		public void UpdatePersons(string personName, int personID)
		{
			Adapter.Update(personName, personID);
		}

		//[DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
		//public bool DeletePersons(int personID)
		//{
		//    int rowsAffected = Adapter.Delete(personID);

		//    // Return true if precisely one row was deleted, otherwise false 
		//    return rowsAffected == 1;
		//}

		[DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
		public void DeletePersons(int personID)
		{
			Adapter.Delete(personID);
		}

	}
}