﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using SAFIS.DAL.SAFISTableAdapters;
using SourceAFIS.Simple;

namespace SAFIS.BLL
{
	[System.ComponentModel.DataObject]
	public class FingerPrintsBLL
	{
		private FingerPrintsTableAdapter _fingerPrintsAdapter = null;
		protected FingerPrintsTableAdapter Adapter
		{
			get
			{
				if (_fingerPrintsAdapter == null)
					_fingerPrintsAdapter = new FingerPrintsTableAdapter();

				return _fingerPrintsAdapter;
			}
		}

		[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
		public SAFIS.DAL.SAFIS.FingerPrintsDataTable GetFingerPrints()
		{
			return Adapter.GetFingerPrints();
		}

		[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
		public SAFIS.DAL.SAFIS.FingerPrintsDataTable GetFingerPrintsByFingerPrintID(int fingerPrintID)
		{
			return Adapter.GetFingerPrintsByFingerPrintID(fingerPrintID);
		}

		[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
		public SAFIS.DAL.SAFIS.FingerPrintsDataTable GetFingerPrintsByPersonID(int personID)
		{
			return Adapter.GetFingerPrintsByPersonID(personID);
		}

		[DataObjectMethodAttribute(DataObjectMethodType.Insert, false)]
		public void InsertFingerPrints(string personName, int fingerID, string fingerPrintPath, byte[] fingerPrintImage)
		{
			//int numberOfScan = getNumberOfScan();
			int numberOfScan = 1;

			//PersonsBLL personsBLL = new PersonsBLL();
			//int personID = personsBLL.InsertPersonsByPersonName(personName);

			PersonsBLL personsBLL = new PersonsBLL();
			personsBLL.InsertPersons(personName);
			SAFIS.DAL.SAFIS.PersonsDataTable personsDataTable = personsBLL.GetMaxPersonID();
			SAFIS.DAL.SAFIS.PersonsRow personsRow = personsDataTable[0];
			int personID = personsRow.PersonID;

			Adapter.Insert(personID, fingerID, numberOfScan, fingerPrintPath, fingerPrintImage);
		}

		[DataObjectMethodAttribute(DataObjectMethodType.Insert, false)]
		public void IdentifyFingerPrints(string personName, int fingerID, string fingerPrintPath, byte[] fingerPrintImage)
		{
		}

		[DataObjectMethodAttribute(DataObjectMethodType.Update, false)]
		public void UpdateFingerPrints(int personID, string personName, int fingerID, int numberOfScan, string fingerPrintPath, byte[] fingerPrintImage, int fingerPrintID)
		{
			if (personName != null)
			{
				PersonsBLL personsBLL = new PersonsBLL();
				personsBLL.UpdatePersons(personName, personID);
			}

			if (fingerID == -1)
			{
				if (fingerPrintPath != null && fingerPrintImage != null)
				{
					UpdateFingerPrintImageByFingerPrintID(fingerPrintPath, fingerPrintImage, fingerPrintID);
				}
			}
			else
			{
				if (fingerPrintPath == null && fingerPrintImage == null)
				{
					UpdateFingerIDByFingerPrintID(fingerID, fingerPrintID);
				}
				else if (fingerPrintPath != null && fingerPrintImage != null)
				{
					UpdateFingerIDFingerPrintImageByFingerPrintID(fingerID, fingerPrintPath, fingerPrintImage, fingerPrintID);
				}
			}
		}

		[DataObjectMethodAttribute(DataObjectMethodType.Update, false)]
		public void UpdateFingerPrintImageByFingerPrintID(string fingerPrintPath, byte[] fingerPrintImage, int fingerPrintID)
		{
			Adapter.UpdateFingerPrintImageByFingerPrintID(fingerPrintPath, fingerPrintImage, fingerPrintID);
		}

		[DataObjectMethodAttribute(DataObjectMethodType.Update, false)]
		public void UpdateFingerIDByFingerPrintID(int fingerID, int fingerPrintID)
		{
			Adapter.UpdateFingerIDByFingerPrintID(fingerID, fingerPrintID);
		}

		[DataObjectMethodAttribute(DataObjectMethodType.Update, false)]
		public void UpdateFingerIDFingerPrintImageByFingerPrintID(int fingerID, string fingerPrintPath, byte[] fingerPrintImage, int fingerPrintID)
		{
			Adapter.UpdateFingerIDFingerPrintImageByFingerPrintID(fingerID, fingerPrintPath, fingerPrintImage, fingerPrintID);
		}

		//[DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
		//public bool DeleteFingerPrints(int fingerPrintID, int personID)
		//{
		//  int rowsAffected = Adapter.Delete(fingerPrintID);

		//  PersonsBLL personsBLL = new PersonsBLL();
		//  personsBLL.DeletePersons(personID);

		//  // Return true if precisely one row was deleted, otherwise false 
		//  return rowsAffected == 1;
		//}

		[DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
		public void DeleteFingerPrints(int fingerPrintID, int personID)
		{
			Adapter.Delete(fingerPrintID);

			PersonsBLL personsBLL = new PersonsBLL();
			personsBLL.DeletePersons(personID);
		}

		[DataObjectMethodAttribute(DataObjectMethodType.Delete, false)]
		public void DeleteFingerPrints(int fingerPrintID)
		{
			FingerPrintsBLL fingerPrintsBLL = new FingerPrintsBLL();
			SAFIS.DAL.SAFIS.FingerPrintsDataTable fingerPrintsDataTable = fingerPrintsBLL.GetFingerPrintsByFingerPrintID(fingerPrintID);
			SAFIS.DAL.SAFIS.FingerPrintsRow fingerPrintsRow = fingerPrintsDataTable[0];

			Adapter.Delete(fingerPrintID);

			PersonsBLL personsBLL = new PersonsBLL();
			personsBLL.DeletePersons(fingerPrintsRow.PersonID);
		}

	}
}