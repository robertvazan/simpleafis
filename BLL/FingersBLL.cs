﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SAFIS.DAL.SAFISTableAdapters;
using System.ComponentModel;

namespace SAFIS.BLL
{
	[System.ComponentModel.DataObject]
	public class FingersBLL
	{
		private FingersTableAdapter _fingersAdapter = null;

		protected FingersTableAdapter Adapter
		{
			get
			{
				if (_fingersAdapter == null)
					_fingersAdapter = new FingersTableAdapter();

				return _fingersAdapter;
			}
		}

		[DataObjectMethodAttribute(DataObjectMethodType.Select, false)]
		public SAFIS.DAL.SAFIS.FingersDataTable GetFingers()
		{
			return Adapter.GetFingers();
		}

	}
}