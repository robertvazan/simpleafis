﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SAFIS.BLL;
using System.IO;

namespace SAFIS.Admin
{
  public partial class WebForm1 : System.Web.UI.Page
  {
		// A page variable to "remember" the deleted fingerprint file
		string deletedFingerPrintPath;

		protected void Page_Load(object sender, EventArgs e)
    {
			//ExceptionDetails.Visible = false;
		}

    protected void NewFingerPrint_ItemInserting(object sender, DetailsViewInsertEventArgs e)
    {
      // Reference the FileUpload controls
      FileUpload insertFingerPrintImageFileUpload = (FileUpload)NewFingerPrint.FindControl("InsertFingerPrintImageFileUpload");

      if (insertFingerPrintImageFileUpload.HasFile)
      {
        // Make sure the picture upload is valid
        if (ValidFingerPrintUpload(insertFingerPrintImageFileUpload))
        {
            const string FingerPrintDirectory = "~/Images/";
						//string fingerPrintPath = FingerPrintDirectory + insertFingerPrintImageFileUpload.FileName;
						deletedFingerPrintPath = FingerPrintDirectory + insertFingerPrintImageFileUpload.FileName;
						string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(insertFingerPrintImageFileUpload.FileName);
            string fileNameExtension = Path.GetExtension(insertFingerPrintImageFileUpload.FileName);

            int iteration = 1;
						while (File.Exists(Server.MapPath(deletedFingerPrintPath)))
            {
							deletedFingerPrintPath = string.Concat(FingerPrintDirectory, fileNameWithoutExtension, "-", iteration, fileNameExtension);
								iteration++;
            }

            // Save the file to disk
						insertFingerPrintImageFileUpload.SaveAs(Server.MapPath(deletedFingerPrintPath));

            // Set the value of FingerPrint parameters
						e.Values["fingerPrintPath"] = deletedFingerPrintPath;
            e.Values["fingerPrintImage"] = insertFingerPrintImageFileUpload.FileBytes;
        }
        else
        {
          // Invalid file upload, cancel insert and exit event handler
          e.Cancel = true;
          return;
        }
      }
      else
      {
        // No picture uploaded!
				ExceptionDetails.Text = "You must provide an image for enrollment.";
				ExceptionDetails.Visible = true;
        e.Cancel = true;
        return;
      }
    }

    protected void NewFingerPrint_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
    {
      if (e.Exception != null)
      {
        // Need to delete fingerprint file, if it exists
				//if (e.Values["fingerPrintPath"] != null)
				//  File.Delete(Server.MapPath(e.Values["fingerPrintPath"].ToString()));

				DeleteRememberedFingerPrintPath();
			}
    }

    protected void FingerPrints_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
      FingerPrintsBLL fingerPrintsBLL = new FingerPrintsBLL();
      SAFIS.DAL.SAFIS.FingerPrintsDataTable fingerPrintsDataTable = fingerPrintsBLL.GetFingerPrintsByFingerPrintID(Convert.ToInt32(e.Keys["FingerPrintID"]));
      SAFIS.DAL.SAFIS.FingerPrintsRow fingerPrintsRow = fingerPrintsDataTable[0];

      if (fingerPrintsRow.IsFingerPrintPathNull())
          deletedFingerPrintPath = null;
      else
          deletedFingerPrintPath = fingerPrintsRow.FingerPrintPath;
    }

    protected void FingerPrints_RowDeleted(object sender, GridViewDeletedEventArgs e)
    {
      // Delete the fingerprint file if there were no problems deleting the record
      if (e.Exception == null)
      {
          DeleteRememberedFingerPrintPath();
      }
    }

    protected void FingerPrints_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
      // Reference the EditFingerPrintImageFileUpload FileUpload
      FileUpload editFingerPrintImageFileUpload = (FileUpload)FingerPrints.Rows[e.RowIndex].FindControl("EditFingerPrintImageFileUpload");

      if (editFingerPrintImageFileUpload.HasFile)
      {
        // Make sure the picture upload is valid
        if (ValidFingerPrintUpload(editFingerPrintImageFileUpload))
        {
          const string FingerPrintDirectory = "~/Images/";
          string fingerPrintPath = FingerPrintDirectory + editFingerPrintImageFileUpload.FileName;
          string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(editFingerPrintImageFileUpload.FileName);
          string fileNameExtension = Path.GetExtension(editFingerPrintImageFileUpload.FileName);

          int iteration = 1;
          while (File.Exists(Server.MapPath(fingerPrintPath)))
          {
              fingerPrintPath = string.Concat(FingerPrintDirectory, fileNameWithoutExtension, "-", iteration, ".", fileNameExtension);
              iteration++;
          }

          // Save the file to disk and set the value of the brochurePath parameter
          editFingerPrintImageFileUpload.SaveAs(Server.MapPath(fingerPrintPath));

          e.NewValues["fingerPrintPath"] = fingerPrintPath;
          e.NewValues["fingerPrintImage"] = editFingerPrintImageFileUpload.FileBytes;
        }
        else
        {
          // Invalid file upload, cancel update and exit event handler
          e.Cancel = true;
          return;
        }
      }
      //else
      //{
      //    // No picture uploaded!
      //    e.NewValues["fingerPrintPath"] = null;
      //    e.NewValues["fingerPrintImage"] = null;
      //}

      // Get FingerPrintID
      //e.NewValues["fingerPrintID"] = Convert.ToInt32(e.Keys["FingerPrintID"]);

      FingerPrintsBLL fingerPrintsBLL = new FingerPrintsBLL();
      SAFIS.DAL.SAFIS.FingerPrintsDataTable fingerPrintsDataTable = fingerPrintsBLL.GetFingerPrintsByFingerPrintID(Convert.ToInt32(e.Keys["FingerPrintID"]));
      SAFIS.DAL.SAFIS.FingerPrintsRow fingerPrintsRow = fingerPrintsDataTable[0];

      // Get PersonID
      //e.NewValues["personID"] = Convert.ToInt32(e.Keys["PersonID"]);
      e.NewValues["personID"] = fingerPrintsRow.PersonID;

      // Reference the PersonNameTextBox TextBox
      TextBox personNameTextBox = (TextBox)FingerPrints.Rows[e.RowIndex].FindControl("PersonNameTextBox");
      string personName = personNameTextBox.Text;
      //if (personName == Convert.ToString(e.Keys["PersonName"]))
      if (fingerPrintsRow.IsPersonNameNull())
      {
        e.NewValues["personName"] = personName;
      }
      else
      {
        if (personName == fingerPrintsRow.PersonName)
            e.NewValues["personName"] = null;
        else
            e.NewValues["personName"] = personName;
      }

      // Reference the EditFingersDropDownList DropDownList
      DropDownList editFingersDropDownList = (DropDownList)FingerPrints.Rows[e.RowIndex].FindControl("EditFingersDropDownList");
      int fingerID = Convert.ToInt32(editFingersDropDownList.SelectedValue);
      //if (fingerID == Convert.ToInt32(e.Keys["FingerID"]))
      if (fingerID == fingerPrintsRow.FingerID)
        e.NewValues["fingerID"] = -1;
      else
        e.NewValues["fingerID"] = fingerID;

      // Get NumberOfScan
      //e.NewValues["numberOfScan"] = null;

      // "Remember" that we need to delete the old fingerprint image file
      if (e.NewValues["fingerPrintPath"] == null)
        deletedFingerPrintPath = null;
      else
        deletedFingerPrintPath = fingerPrintsRow.FingerPrintPath;
    }

    protected void FingerPrints_RowUpdated(object sender, GridViewUpdatedEventArgs e)
    {
      // If there were no problems and we updated the fingerprint image file, then delete the existing one
      if (e.Exception == null)
      {
        DeleteRememberedFingerPrintPath();
      }
    }

		protected string GenerateFingerPrintLink(object fingerPrintPath)
		{
			//if (Convert.IsDBNull(fingerPrintPath))
			//  return "No FingerPrint Available";
			//else
			//  //return string.Format(@"<a href=""{0}"">View FingerPrint</a>", ResolveUrl(FingerPrintPath.ToString()));
			return string.Format(@"{0}", ResolveUrl(fingerPrintPath.ToString()));
		}

		private bool ValidFingerPrintUpload(FileUpload fileUpload)
    {
      // Make sure that a JPG, TIF has been uploaded
      if (string.Compare(Path.GetExtension(fileUpload.FileName), ".jpg", true) != 0 &&
        string.Compare(Path.GetExtension(fileUpload.FileName), ".jpeg", true) != 0 &&
        string.Compare(Path.GetExtension(fileUpload.FileName), ".tif", true) != 0 &&
        string.Compare(Path.GetExtension(fileUpload.FileName), ".tiff", true) != 0)
      {
				ExceptionDetails.Text = "Only JPG, TIF formats may be used.";
				ExceptionDetails.Visible = true;
        return false;
      }
      else
      {
        return true;
      }
    }

    private void DeleteRememberedFingerPrintPath()
    {
      // Is there a file to delete?
      if (deletedFingerPrintPath != null)
      {
        File.Delete(Server.MapPath(deletedFingerPrintPath));
      }
    }

//    protected void UploadButton_Click(object sender, EventArgs e)
//    {
//      if (UploadTest.HasFile == false)
//      {
//        // No file uploaded!
//        UploadDetails.Text = "Please first select a file to upload...";
//      }
//      else
//      {
//        // Display the uploaded file's details
//        UploadDetails.Text = string.Format(
//                @"Uploaded file: {0}<br />
//                          File size (in bytes): {1:N0}<br />
//                          Content-type: {2}",
//                  UploadTest.FileName,
//                  UploadTest.FileBytes.Length,
//                  UploadTest.PostedFile.ContentType);

//        // Save the file
//        string filePath = Server.MapPath("~/Images/" + UploadTest.FileName);
//        UploadTest.SaveAs(filePath);
//      }
//    }

  }
}